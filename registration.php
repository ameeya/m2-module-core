<?php
/**
 * @copyright  Copyright (c) 2017 Ameeya (http://www.ameeya.com)
 * @license    http://www.ameeya.com/LICENSE.txt
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Ameeya_Core',
    __DIR__
);

