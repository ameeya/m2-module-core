# Ameeya Core Module for all our Magento 2 Modules

## How to install Ameeya_Core


#### Install via composer

Run the following command in Magento 2 root folder

```
composer require ameeya/m2-module-core
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy
```

Run compile if your store in Production mode:

```
php bin/magento setup:di:compile
```
